# IMPORTANT

This repository has moved to [https://code.alienscience.org/alienscience/bufstream-fresh](https://code.alienscience.org/alienscience/bufstream-fresh).

---

# bufstream-fresh

This is a fork of the [bufstream crate](https://github.com/alexcrichton/bufstream/issues/13).

Buffered I/O streams for reading/writing.

[Documentation](https://docs.rs/bufstream-fresh/)

## Usage

```toml
[dependencies]
bufstream-fresh = "0.3"
```

